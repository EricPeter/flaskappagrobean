import cv2
import numpy as np
# import imutils
#
# img  = cv2.imread('tiger.jpg')
# hsv = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)
#
# lower_range = np.array([110,50,50])
# upper_range =np.array([130,255,255])
#
# mask = cv2.inRange(hsv,lower_range,upper_range)
# cv2.imshow('image',img)
# cv2.imshow('mask',mask)
#
# while():
#     k = cv2.waitKey(5) & 0xFF
#     if k== 27:
#         break
# cv2.destroyAllWindows()

# ##image read
img = cv2.imread("bn.jpeg")

##convert to hsv
hsv = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)

mask = cv2.inRange(hsv,(36,50,50),(70, 255, 255))

imask = mask>0

green = np.zeros_like(img,np.uint8)
green[imask] = img[imask]
if np.array_equal(mask):
    print("True")
else:
    print("False")

cv2.imwrite("green.png",green)